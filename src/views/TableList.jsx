
import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";
import Papa from 'papaparse';
var allHeaders=[];
var  GeneralData1 = [];
var  GeneralData2 = [];
function myFunction(item, index) {
  let allheader=[
    "payload.InverterDetected",
    "payload.Version",
    "payload.DeviceID",
    "payload.InstallType",
    "payload.BatteryData.MaxChgAlwd",
    "payload.BatteryData.CellMinV",
    "payload.BatteryData.BatChg",
    "payload.BatteryData.CellMaxVId",
    "payload.BatteryData.BatDisChg",
    "payload.BatteryData.SOC",
    "payload.BatteryData.TotalBatVolt",
    "payload.BatteryData.UnVWa",
    "payload.BatteryData.ChgOvA",
    "payload.BatteryData.ClusRe",
    "payload.BatteryData.BatFault",
    "payload.BatteryData.OvVWa",
    "payload.BatteryData.TotalBatCur",
    "payload.BatteryData.NoOfClust",
    "payload.BatteryData.BatMaxTemp",
    "payload.BatteryData.BncF",
    "payload.BatteryData.CellMaxV",
    "payload.BatteryData.BatMinTemp",
    "payload.BatteryData.CellMinVId",
    "payload.BatteryData.MaxDisChgAlwd",
    "payload.BatteryData.MosBncF",
    "payload.BatteryData.DchgOvA",
    "payload.Timestamp",
  
    "payload.InverterData.0.BtCp",
    "payload.InverterData.0.SetCB",
    "payload.InverterData.0.ExBaT",
    "payload.InverterData.0.PVInV1",
    "payload.InverterData.0.PVInV2",
    "payload.InverterData.0.BtV",
    "payload.InverterData.0.ACOF",
    "payload.InverterData.0.GdF",
    "payload.InverterData.0.GdA1",
    "payload.InverterData.0.InverterId", 
    "payload.InverterData.0.BtA",
    "payload.InverterData.0.GdA3",
    "payload.InverterData.0.InT",
    "payload.InverterData.0.GdA2",
    "payload.InverterData.0.GdV2",
    "payload.InverterData.0.PVInA1",
    "payload.InverterData.0.GdV1",
    "payload.InverterData.0.ACOV3",
    "payload.InverterData.0.ACOV2",
    "payload.InverterData.0.GdV3",
    "payload.InverterData.0.ACOV1",
    "payload.InverterData.0.PVInA2",
    "payload.InverterData.0.CoMxT",
  
    "payload.InverterData.1.BtCp",
    "payload.InverterData.1.SetCB",
    "payload.InverterData.1.ExBaT",
    "payload.InverterData.1.PVInV1",
    "payload.InverterData.1.PVInV2",
    "payload.InverterData.1.BtV",
    "payload.InverterData.1.ACOF",
    "payload.InverterData.1.GdF",
    "payload.InverterData.1.GdA1",
    "payload.InverterData.1.InverterId",
    "payload.InverterData.1.BtA",
    "payload.InverterData.1.GdA3",
    "payload.InverterData.1.InT",
    "payload.InverterData.1.GdA2",
    "payload.InverterData.1.GdV2",
    "payload.InverterData.1.PVInA1",
    "payload.InverterData.1.GdV1",
    "payload.InverterData.1.ACOV3",
    "payload.InverterData.1.ACOV2",
    "payload.InverterData.1.GdV3",
    "payload.InverterData.1.ACOV1",
    "payload.InverterData.1.PVInA2",
    "payload.InverterData.1.CoMxT",
  
    "payload.EnergyData.0.ConMfGd",
    "payload.EnergyData.0.GnM",
    "payload.EnergyData.0.GnT",
    "payload.EnergyData.0.ConWfGd",
    "payload.EnergyData.0.InverterId",
    "payload.EnergyData.0.ConYfGd",
    "payload.EnergyData.0.ConTfGd",
    "payload.EnergyData.0.GnW",
    "payload.EnergyData.0.GnY",
    "payload.EnergyData.0.PkM",
    "payload.EnergyData.0.ConWOGd",
    "payload.EnergyData.0.ConYOGd",
    "payload.EnergyData.0.PkT",
    "payload.EnergyData.0.ConTOGd",
    "payload.EnergyData.0.ConMOGd",
    "payload.EnergyData.0.PkW",
    "payload.EnergyData.0.PkY",
    
    "payload.EnergyData.1.ConMfGd",
    "payload.EnergyData.1.GnM",
    "payload.EnergyData.1.GnT",
    "payload.EnergyData.1.ConWfGd",
    "payload.EnergyData.1.InverterId", 
    "payload.EnergyData.1.ConYfGd",
    "payload.EnergyData.1.ConTfGd",
    "payload.EnergyData.1.GnW",
    "payload.EnergyData.1.GnY",
    "payload.EnergyData.1.PkM",
    "payload.EnergyData.1.ConWOGd",
    "payload.EnergyData.1.ConYOGd",
    "payload.EnergyData.1.PkT",
    "payload.EnergyData.1.ConTOGd",
    "payload.EnergyData.1.ConMOGd",
    "payload.EnergyData.1.PkW",
    "payload.EnergyData.1.PkY",
    "payload.MiscData.InstallSize",
    "payload.MiscData.InvOffBy",
    "payload.MiscData.ChgCycle",
    "payload.MiscData.ExptdChgCyl",
    "payload.MiscData.InvState",
    "DeviceTimeStamp",
    "DeviceID",                                 
    "payload.ModeData.0.InverterId",
    "payload.ModeData.0.Mode",
    "payload.ModeData.1.InverterId",
    "payload.ModeData.1.Mode"
  ]
  var element = {};
  var Generalelement = {};

  var Batteryelement = {};
  var Inverterelement1 = {};
  var Inverterelement2 = {};
  var Energyelement1 = {};
  var Energyelement2 = {};
  var Generalelement_row = [];
  var Batteryelement_row = [];
  var Inverterelement1_row = [];
  var Inverterelement2_row = [];
  var  Energyelement1_row = [];
  var  Energyelement2_row = [];
  var  GeneralInverter = [];
  var  GeneralEnergy = [];
 
  // for(let i=0;i<=30;i++){
  for(let i=0;i<allheader.length;i++){
  
  let label=allheader[i]
  // console.log(label)
let index = allHeaders.indexOf(label);
let This_value=item[index];
let namearray=label.split(".");
// console.log(namearray)
let key=label;
let indo=namearray.length-1;
    key =namearray[indo];

// if(namearray.length===4){
//     key =namearray[3];
// }
// else if(namearray.length===3){
//     key =namearray[2];
// }


if(index <=3 ){
   
  Generalelement[key]= This_value;
}
else if(index <=26 ){
  Batteryelement[key]= This_value;
  // Batteryelement_row.push(Batteryelement);
}
else if(index <=50 ){
   Inverterelement1[key]= This_value;
  // Inverterelement1_row.push(Inverterelement1);

}
else if(index <=72 ){
 
  Inverterelement2[key]= This_value;
  // Inverterelement2_row.push(Inverterelement2);

}
else if(index <=89 ){
 
  Energyelement1[key]= This_value;
  // Energyelement1_row.push(Energyelement1);

}
else if(index <=106 ){
 
  Energyelement2[key]= This_value;
  // Energyelement2_row.push(Energyelement2);

}
else{
  Generalelement[key]= This_value;
  // Generalelement_row.push(Generalelement);

}

console.log(index);
console.log(This_value);


// element[label]= This_value;
// element.quantity = quantity;
// singlerow.push(element);

  }
  console.log(Generalelement)
  console.log('Generalelement')
  Generalelement_row.push(Generalelement);
  Batteryelement_row.push(Batteryelement);
  Energyelement2_row.push(Energyelement2);
  Energyelement1_row.push(Energyelement1);
   Inverterelement2_row.push(Inverterelement2);
  Inverterelement1_row.push(Inverterelement1);
  let  inverterfulldata = {...Generalelement,...Batteryelement,...Inverterelement1,...Energyelement1 }
  let  inverterfulldata2 = {...Generalelement,...Batteryelement,...Inverterelement1,...Energyelement1 }
  GeneralData1.push(inverterfulldata);
  GeneralData2.push(inverterfulldata2);
  let getheader={...inverterfulldata,...inverterfulldata2}
  // GeneralInverter2.push(inverterfulldata2);
  // GeneralEnergy.push(Energyelement2);
  // GeneralEnergy.push(Energyelement1);


  console.log(Generalelement_row)
  console.log(index+"<<<<<<<>>>>>>>")
  console.log(getheader)
  console.log("getheader")

  // console.log(Batteryelement_row)
  // console.log(Inverterelement1_row)
  // console.log(Inverterelement2_row)
  // console.log(Energyelement1_row)
  // console.log(Energyelement2_row)

  // console.log('GeneralInverter')
  // console.log(GeneralInverter)
  console.log(GeneralData1)
  console.log(GeneralData2)


   
  console.log(item)
 }
class TableList extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data: [],
        general1:[],
        general2:[],
        CombinedRecord:[],
        headerkeys:[],
    };

    this.getData = this.getData.bind(this);
}
  componentWillMount() {
    this.getCsvData();
}

fetchCsv() {
    return fetch('/data/data.csv').then(function (response) {
        let reader = response.body.getReader();
        let decoder = new TextDecoder('utf-8');

        return reader.read().then(function (result) {
            return decoder.decode(result.value);
        });
    });
}

getData(result) {
  console.log(result)

  let allrecord=result.data;
    allHeaders=allrecord[0];

  console.log(allHeaders)
let inverters1=[];
let inverters2=[];
let NewRecords=allrecord.slice(1, 150);
NewRecords.forEach(myFunction);
//let  Combo = [];
// Combo.push(GeneralData1)
// Combo.push(GeneralData2)
var Combo = [...GeneralData1, ...GeneralData2];
let first=GeneralData1[0];
let second=GeneralData2[0];
let hhhh={...first,...second};
console.log(hhhh);
console.log('hhhh>>>');
let keys=Object.keys(hhhh)
console.log(keys)
console.log("keys")
this.setState({
  data: result.data,
general1:GeneralData1,
general2:GeneralData2,
CombinedRecord:Combo,
headerkeys:keys
}
);

console.log('CombinedRecord')
console.log(this.state.CombinedRecord)
console.log('CombinedRecord')
}




async getCsvData() {
  console.log("heyman ")
    let csvData = await this.fetchCsv();
    //  console.log(csvData)
    Papa.parse(csvData, {
        complete: this.getData
    });
}

  render() {
const {CombinedRecord,headerkeys}=this.state;









    
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Device Record"
                category=""
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover responsive style={{overflow: 'auto'}}>
                    <thead responsive>
                      <tr>
                        {headerkeys.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {
                      // let keys=Object.keys(item)
                      CombinedRecord.map((item, key) => {
                         console.log(item)
                         console.log(key)
                         let keys=Object.keys(item)
                         return (
       
                          <tr>
                        {  keys.map((i) => {
                              let keys=Object.keys(item)
                            //  console.log("great God")
                            //  console.log(keys);
                             return (
                            
                              <td>
                                {item[i]}
                              </td>
                           );
                          }) 
                        }
                          </tr>
                          );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default TableList;
